#!/bin/bash
set -e
mysql --protocol=socket -uroot <<EOSQL
CREATE DATABASE laravel;
CREATE USER 'laravel'@'localhost';
GRANT ALL ON laravel.* TO 'laravel'@'localhost';
EOSQL
