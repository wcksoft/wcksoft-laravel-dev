#/bin/bash
LOCKFILE="/srv/wck.lock"

sudo service mysql start
sudo service redis-server start

if [ ! -e $LOCKFILE ]; then
    sudo touch $LOCKFILE
    echo " Configuring database..."
    sudo /srv/dbsetup.sh
else
    echo " Skipping database setup..."
fi

php artisan key:generate
npm run dev
php artisan serve --host=0.0.0.0
