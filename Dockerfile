# Base Debian 10 slim
FROM debian:buster-slim
# set terminal to non interactive, it supress some warning messages from the output.
RUN echo 'debconf debconf/frontend select Noninteractive' | debconf-set-selections
## Install all dependencies
RUN apt-get update && \
	apt-get install -y --no-install-recommends apt-utils && \
	apt-get -y install curl wget lsb-release apt-transport-https ca-certificates \
		mariadb-server mariadb-client redis gcc g++ make sudo nano
# PHP 7.4, composer & phpunit
RUN wget -O /etc/apt/trusted.gpg.d/php.gpg https://packages.sury.org/php/apt.gpg && \
	echo "deb https://packages.sury.org/php/ $(lsb_release -sc) main" | tee /etc/apt/sources.list.d/php.list && \
	apt-get update && \
	apt-get -y install php7.4-fpm php7.4-mysql php7.4-cli php7.4-zip php7.4-xml composer phpunit \
		php-redis
# NodeJS 14
RUN curl -sL https://deb.nodesource.com/setup_14.x | bash - && \
	apt-get install -y nodejs
# Upgrade Debian packages (security)
RUN apt-get update && apt-get -y upgrade
# Copy entry scripts
WORKDIR /srv
COPY dbsetup.sh ./
COPY start.sh ./
# Setup Laravel folder then Add & switch to laravel user
RUN mkdir laravel && \
	useradd -ms /bin/bash laravel && \
	adduser laravel sudo && \
	chown laravel:laravel laravel && \
	echo "laravel	ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers
USER laravel
WORKDIR /srv/laravel
# Install Laravel then Install dependecies via composer & npm
RUN composer global require laravel/installer && \
	composer create-project --prefer-dist laravel/laravel . 
COPY env .env
RUN sudo chown laravel:laravel .env && \
	composer update && \
	npm install
# Expose our web port and run the webserver.
EXPOSE 8000/tcp
ENTRYPOINT exec /srv/start.sh
